fn main() {
    // Get input with image crate from rgb16 png file
    let in_img = image::open("/tmp/test.png").expect("/tmp/test.png not found");

    assert!(in_img.color() == image::ColorType::Rgb16);
    println!("{:?}", in_img.color());

    // fails
    // let out_img = in_img.thumbnail(220, 220);
    // works, bad qual
    let out_img = in_img.resize(220, 220, image::imageops::FilterType::Nearest);
    // fails
    //let out_img = in_img.resize(220, 220, image::imageops::FilterType::Triangle);
    // fails
    // let out_img = in_img.resize(220, 220, image::imageops::FilterType::CatmullRom);
    // fails
    // let out_img = in_img.resize(220, 220, image::imageops::FilterType::Gaussian);
    // fails
    // let out_img = in_img.resize(220, 220, image::imageops::FilterType::Lanczos3);

    assert!(out_img.color() == image::ColorType::Rgb16);
    println!("{:?}", out_img.color());

    // Write it with image
    out_img
        .save("/tmp/test-output-image.png")
        .expect("Cannot create output file");

    // Write it with image-png
    let file = std::fs::File::create("/tmp/test-output-png-encoder.png")
        .expect("Cannot create output file");
    let ref mut writer = std::io::BufWriter::new(file);

    let mut encoder = png::Encoder::new(writer, out_img.width(), out_img.height());
    encoder.set_color(png::ColorType::Rgb);
    encoder.set_depth(png::BitDepth::Sixteen);

    let mut writer = encoder.write_header().unwrap();
    writer
        .write_image_data(&out_img.as_bytes().to_vec())
        .unwrap();
}
